<?php
/**
 * Rules regular expressions
 *
 * Provides simple and powerful regular expression actions.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_regex_rules_action_info() {
  $description_pattern= t('The <a href="@regex">regular expression</a> to search for using <a href="@preg_match_all">preg_match_all()</a>, with enclosing <a href="@delimiters">delimiters</a> and maybe <a href="@modifiers">modifiers</a>, as a string. In most cases you want to append the "u" (unicode) modifier. Example: "/^f(o+)bar/u". Tip: <a href="@RegExr">RegExr: Online Regular Expression Testing Tool</a> is helpful for learning, writing, finding, and testing Regular Expressions.',
    array(
      '@regex' => 'http://www.php.net/manual/en/reference.pcre.pattern.syntax.php',
      '@preg_match_all' => 'http://php.net/manual/de/function.preg-match-all.php',
      '@delimiters' => 'http://www.php.net/manual/en/regexp.reference.delimiters.php',
      '@modifiers' => 'http://www.php.net/manual/en/reference.pcre.pattern.modifiers.php',
      '@RegExr' => 'http://gskinner.com/RegExr/',
    )
  );
  $description_replacement = t('The replacement string may contain numeric (not named) backreferences to subpatterns of the form \\1, $1, or ${1}.');
  $description_pattern_multiple = t('If set, enter a list of pattern values, each on one line. Each matching pattern is replaced with the replacement (for single replacement) or corresponding replacement (for multiple replacements).');
  $description_replacement_multiple = t('If set, enter multiple replacement values, each on one line. Only allowed for multiple patterns. Each matching pattern is replaced by the corresponding replacement');
  $description_string = t('The string on which to run the regular expression.');
  $description_offset = t('The zero-based offset to search from.');
  $subpattern_args = array('@subpatterns' => 'http://www.php.net/manual/en/regexp.reference.subpatterns.php');
  $description_subpattern_name = t('Numeric or named <a href="@subpatterns">subpattern</a> to return. Defaults to 0 for whole match.', $subpattern_args);
  $description_subpattern_names = t('Return subpatterns in the order given here (instead of the order they appear in the regular expression). Each line must contain the name of a named <a href="@subpatterns">subpattern</a> (like "(?&lt;name&gt;.*)") or its numeric index (starting with 1).', $subpattern_args);
  $description_result_string = t('The result string with matches replaced.');
  $description_result_count = t('The number of replacements done.');
  $description_match = t('The first match of the search.');
  $description_all_matches = t('The matches of the search as list of list of strings, indexed by match, then by subpattern, starting with 0 for the whole match. So all-matches:0:0 is the whole first match, all-matches:1:7 is the 7th subpattern of the 2nd match.');
  $description_limit = t('The maximum possible replacements for each pattern in each subject string. A value of -1 means no limit.');

  $return['data_regex_replace'] = array(
    'label' => t('Search and replace text by regular expression'),
    'group' => t('Regular expressions'),
    'base' => 'rules_regex_action_regex_replace',
    'parameter' => array(
      'pattern' => array(
        'type' => 'text',
        'label' => t('Pattern'),
        'description' => $description_pattern,
        'default value' => '/.*/u',
      ),
      'pattern_multiple' => array(
        'type' => 'boolean',
        'label' => t('Multiple patterns'),
        'description' => $description_pattern_multiple,
      ),
      'replacement' => array(
        'type' => 'text',
        'label' => t('Replacement'),
        'description' => $description_replacement,
        'optional' => TRUE,
      ),
      'replacement_multiple' => array(
        'type' => 'boolean',
        'label' => t('Multiple replacements'),
        'description' => $description_replacement_multiple,
      ),
      'string' => array(
        'type' => 'text',
        'label' => t('String'),
        'description' => $description_string,
      ),
      'limit' => array(
        'type' => 'integer',
        'label' => t('Limit'),
        'description' => $description_limit,
        'default value' => -1,
      ),
    ),
    'provides' => array(
      'result_string' => array(
        'type' => 'text',
        'label' => t('Result string'),
        'description' => $description_result_string,
      ),
      'result_count' => array(
        'type' => 'integer',
        'label' => t('Result count'),
        'description' => $description_result_count,
      ),
    ),
    // @todo validate expression via @preg_match($e, null) === false
  );
  $return['data_regex_simple'] = array(
    'label' => t('Search text by regular expression (simple)'),
    'group' => t('Regular expressions'),
    'base' => 'rules_regex_action_regex_simple',
    'parameter' => array(
      'pattern' => array(
        'type' => 'text',
        'label' => t('Pattern'),
        'description' => $description_pattern,
        'default value' => '/.*/u',
      ),
      'string' => array(
        'type' => 'text',
        'label' => t('String'),
        'description' => t('The string on which to run the regular expression.'),
      ),
      'offset' => array(
        'type' => 'integer',
        'label' => t('Offset'),
        'description' => $description_offset,
        'default value' => 0,
      ),
      'subpattern_name' => array(
        'type' => 'text',
        'label' => t('Subpattern name'),
        'description' => $description_subpattern_name,
        'default value' => 0,
      ),
    ),
    'provides' => array(
      'match' => array(
        'type' => 'text',
        'label' => t('Match'),
        'description' => $description_match,
      ),
    ),
    // @todo validate expression via @preg_match($e, null) === false
  );
  $return['data_regex_powerful'] = array(
    'label' => t('Search text by regular expression (powerful)'),
    'group' => t('Regular expressions'),
    'base' => 'rules_regex_action_regex_powerful',
    'parameter' => array(
      'pattern' => array(
        'type' => 'text',
        'label' => t('Pattern'),
        'description' => $description_pattern,
        'default value' => '/.*/u',
      ),
      'string' => array(
        'type' => 'text',
        'label' => t('String'),
        'description' => t('The string on which to run the regular expression.'),
      ),
      'offset' => array(
        'type' => 'integer',
        'label' => t('Offset'),
        'description' => $description_offset,
        'default value' => 0,
      ),
      'subpattern_names' => array(
        'type' => 'list<text>',
        'label' => t('Reorder subpatterns'),
        'optional' => TRUE,
        'description' => $description_subpattern_names,
      ),
    ),
    'provides' => array(
      'all_matches' => array(
        'type' => 'list<list<text>>',
        'label' => t('All matches'),
        'description' => $description_all_matches,
      ),
    ),
    // @todo validate expression via @preg_match($e, null) === false
  );
  return $return;
}

/**
 * Action: Search text by regular expression (simple)
 */
function rules_regex_action_regex_replace($pattern, $pattern_multiple, $replacement, $replacement_multiple, $string, $limit, $settings, $state, $element) {
  if ($pattern_multiple) {
    $pattern = preg_split('/\r?\n/u', $pattern);
  }
  if ($replacement_multiple) {
    $replacement = preg_split('/\r?\n/u', $replacement);
  }
  $result_string = preg_replace($pattern, $replacement, $string, $limit, $result_count);
  return array('result_string' => $result_string, 'result_count' => $result_count);
}

/**
 * Action: Search text by regular expression (simple)
 */
function rules_regex_action_regex_simple($pattern, $string, $offset, $subpattern_name, $settings, $state, $element) {
  preg_match($pattern, $string, $raw_matches, 0, $offset);
  if (isset($raw_matches[$subpattern_name])) {
    $match = $raw_matches[$subpattern_name];
  }
  else {
    $match = NULL;
  }
  return array('match' => $match);
}

/**
 * Action: Search text by regular expression (powerful)
 */
function rules_regex_action_regex_powerful($pattern, $string, $offset, $subpattern_names, $settings, $state, $element) {
  preg_match_all($pattern, $string, $all_raw_matches, PREG_SET_ORDER, $offset);
  $all_matches = array();
  if ($subpattern_names) {
    // Iterate over matches.
    foreach($all_raw_matches as $raw_matches) {
      // First element is the whole match.
      $matches = array($raw_matches[0]);
      // Iterate over subpatterns and collect named subpatterns.
      foreach ($subpattern_names as $subpattern_name) {
        if (isset($raw_matches[$subpattern_name])) {
          $matches[] = $raw_matches[$subpattern_name];
        }
        else {
          $matches[] = NULL;
        }
      }
      $all_matches[] = $matches;
    }
  }
  else {
    // Iterate over matches.
    foreach($all_raw_matches as $raw_matches) {
      // Filter out numeric indices so to ignore matches of named subpatterns.
      $numeric_keys = array_filter(array_keys($raw_matches), 'is_numeric');
      $matches = array_intersect_key($raw_matches, array_flip($numeric_keys));
      $all_matches[] = $matches;
    }
  }
  return array('all_matches' => $all_matches);
}
